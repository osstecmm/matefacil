#!/bin/bash
# -*- mode: shell-script; indent-tabs-mode: nil; sh-basic-offset: 4; -*-
# ex: ts=8 sw=4 sts=4 et filetype=sh
#
# SPDX-License-Identifier: GPL-3.0-or-later

sudo apt install \
    graphviz \
    postgresql \
    postgresql-client \
    python3-decouple \
    python3-django \
    python3-graphviz \
    python3-psycopg2 \
    python3-pydot \
    python3-willow \
    openjdk-21-jre-headless \
    ufw

#sudo ufw enable
#sudo ufw allow 80
#sudo ufw route allow from any port 80 to 127.0.0.1 port 8080
