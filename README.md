# MateFacil

## Aplicación Web para la evaluación de las competencias técnicas de los estudiantes del ITJMMPH Unidad Académica Zapopan

<img src="https://i.imgur.com/g0ro1Sl.png" width="45%" height="45%">

### Requerimientos para el entorno de desarrollo local

- Docker
- Python (3.8.3)
  - venv (Entornos virtuales con Python)
  - Django (3.0.5)
  - psycopg2-binary (2.8.5)
  - Pillow (7.1.1)
  - Graphviz
- OpenJDk 14
- G++ (GCC) 10.1.0
- Mathlive

### Pasos para construir la imagen de Docker:

- `docker-compose up --build`
- `docker-compose run app python manage.py makemigrations center`    ------reconstruir base de datos
- `docker-compose run app python manage.py migrate`					 ------reconstruir base de datos
- `docker-compose run app python manage.py createsuperuser` (Aquí se crea la cuenta para el administrador)
- `docker-compose up`
- `http://0.0.0.0:8080/` o `http://localhost:8080/`

### Pasos para construir la imagen en Podman:

- `podman build -t matefacil .`
- `podman network create --label app=matefacil --subnet=192.168.50.0/24 --gateway=192.168.50.1 mfnet`
- `podman volume  create --label app=matefacil mfDBvol`
- ```
podman pod     create --label app=matefacil \
  --infra-name=mfinfra \
  --publish 5432:5432 --publish 8080:8080 --network=mfnet --name mfpod
```
- Use the script from `extra/podman-quadlet/01-crear-secretos.sh` to create the
  podman sectrets.
- ```
podman run -d \
  --secret=postgres-user,type=env,target=POSTGRES_USER \
  --secret=postgres-password,type=env,target=POSTGRES_PASSWORD \
  --secret=postgres-db,type=env,target=POSTGRES_DB \
  --volume mfDBvol:/var/lib/postgresql/data:Z \
  --restart=unless-stopped  \
  --name db \
  --pod mfpod \
  postgres:15.3
```
- Ejecutar los siguientes comandos para entrar en modo interativo en el
  contendedor
  ```
podman run --pod mfpod \
  -i -t --rm \
  -v ./program:/home/program:Z \
  --name matefacil matefacil:latest
  ```
  En el shell del contenedor ejecutar:
  - `python manage.py makemigrations center`    ------reconstruir base de datos
  - `python manage.py migrate`					 ------reconstruir base de datos
  - `python manage.py createsuperuser` (Aquí se crea la cuenta para el administrador)
  - `exit` para salir del contenedor
- Finalmente ejecutar
  ```
podman run -d --pod mfpod \
  -v ./program:/home/program:Z \
  --name matefacil \
  matefacil:latest \
  bash -c "python manage.py runserver 0.0.0.0:8080"
  ```
<!--
- `podman inspect matefacil-db -f '{{ $network := index .NetworkSettings.Networks "matefacil-net" }}{{ $network.IPAddress}}' `
-->

### Pasos para configurar la plataforma desde cero (para el administrador):

- Iniciar sesión como administrador: `http://0.0.0.0:8080/admin`
- Selecionar Groups
- Crear grupo `teacher`:
  - **(en teacher en permisos debes de agregar: _center | classes | can add classes_)**
- Crear grupo `student`:
  - **(permisos para el grupo: center | averages work activity | Can add averages work activity)**
  - **( center | classrooms | Can add classrooms)**
- Selecionar `Type classs`
  - Agregar en este orden: Aritmética, Desigualdad, Tabla de verdad, Grafos
- Selecionar `Type activitys`
  - Agregar Homework (o Tarea) y Exam (o Examen)

```sql 
INSERT INTO public.auth_group (name) VALUES ('teacher'),('student');
INSERT INTO public.auth_group_permissions (group_id, permission_id) VALUES (1,25),(2,61);
INSERT INTO public.center_typeclass (name) VALUES ('Aritmética'),('Desigualdad'),('Tabla de verdad');
INSERT INTO public.center_typeactivity (name) VALUES ('Tarea'),('Examen');
```

Reconstruir contenedor: docker-compose up --build 

### TODO

- [ ] Realizar correcciones del código existente,en busca de hacer el código más entendible entre el equipo de trabajo.
- [ ] Mejorar la interfazg ráfica, actualmente se está usando Bootstrap debido a que funciona sin configuraciones complejas, considerar usar algo mejor a lo existente.
- [ ] Agregar un módulo para poder guardar los ejercicios y/o exámenes en documentos descargables que son generados através de un intérprete de LáteX (aquí se pueden utilizar librerías que ya están listas para resolver esta problemática).
- [ ] Utilizar las herramientas de integración continua y despliegue continuo (CI/CD) através de la plataforma GitLab, que, dicha plataforma ya cuenta con estas funcionalidades listas para integrarse a cualquier proyecto.
- [ ] Agregar entornos de pruebas para corroborar el funcionamiento y la calidad del código, utilizando herramientas como Tox y Coverage.
- [ ] Agregar más generadores y validadores de problemas matemáticos.

### Acerca de

Este proyecto se comenzó en la Unidad Académica Zapopan del ITJMMPH dentro del periodo de residencia profesional Febrero - Julio 2020.

Estudiantes:

- Luis Manuel Chávez Velasco				Febrero 2020 - Julio 2020
- [Jesús Castro](https://github.com/jcstr)	Febrero 2020 - Julio 2020
- Axel Michael Maytorena Ramírez			Agosto  2020 - Enero 2021
- Josué Esaud Gómez González				Agosto  2020 - Enero 2021
- Jose David Iñiguez Lopez					Febrero 2021 - Julio 2021
- Luis A. M. Orellan S.						Febrero 2021 - Julio 2021

Asesores:

- Ph. D. Miguel Bernal Marin
- M.S.C. Alejandro Aguilar Cornejo
- M.S.C. Miriam Díaz Rogríguez

LICENSE: GPLv3


