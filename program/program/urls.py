"""program URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib.auth import views as auth_views
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path
from center import views

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.index, name='index'),
    path('profile/', views.profile, name='profile'),
    path('profile/pass/', views.changePass, name='pass'),
    path('profile/deleteUser/', views.deleteUser, name='deleteUser'),
    path('listPractices/', views.listPractices, name='listPractices'),
    path('listPractices/createPractice/', views.createPractice, name='createPractice'),
    path('listPractices/createPractice/practice/<int:pk>/', views.practice, name='practice'),
    path('listPractices/createPractice/practice/<int:pk>/validationPractice/', views.validationPractice, name='validationPractice'),
    path('listPractices/createPractice/generation', views.generationPractice, name='generationPractice'),
    path('createClass/', views.createClass, name='createClass'),
    path('listCalassrooms/', views.listCalassrooms, name='listCalassrooms'),
    path('listCalassrooms/validCode/', views.validCode, name='validCode'),
    path('listCalassrooms/<str:name>/', views.classroom, name='classroom'),
    path('listCalassrooms/<str:name>/members/', views.members, name='members'),
    path('listCalassrooms/<str:name>/listExam/', views.listExam, name='listExam'),
    path('listCalassrooms/<str:name>/listHomework/', views.listHomework, name='listHomework'),
    path('listCalassrooms/<str:name>/listExam/createExam/', views.createExam, name='createExam'),
    path('listCalassrooms/<str:name>/listHomework/createHomework/', views.createHomework, name='createHomework'),
    path('listCalassrooms/<str:name>/generationWork/', views.generationWork, name='generationWork'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/', views.work, name='workHomework'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/', views.work, name='workExam'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/homework/', views.homeworkOrExam, name='homework'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/exam/', views.homeworkOrExam, name='exam'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/homework/valid/', views.validHomeworkOrExam, name='homework'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/exam/valid/', views.validHomeworkOrExam, name='exam'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/viewAnswer/<str:student_id>', views.viewAnswer, name='viewAnswer'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/viewAnswer/<str:student_id>', views.viewAnswer, name='viewAnswer'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/homework/viewAnswer/<str:student_id>', views.viewAnswer, name='viewAnswer'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/homework/viewAnswer/<str:student_id>', views.viewAnswer, name='viewAnswer'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/editWorkHome/', views.editHomeworkOrExam, name='editWorkHome'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/editWorkExam/', views.editHomeworkOrExam, name='editWorkExam'),
    path('listCalassrooms/<str:name>/listExam/<str:name_work>/editWorkExam/deleteExam/', views.deleteHomeworkOrExam, name='deleteExam'),
    path('listCalassrooms/<str:name>/listHomework/<str:name_work>/editWorkHome/deleteHome/', views.deleteHomeworkOrExam, name='deleteHome'),
    path('listCalassrooms/<str:name>/profileClass/', views.profileClass, name='profileClass'),
    path('listCalassrooms/<str:name>/profileClass/deleteClass/', views.deleteClass, name='deleteClass'),
    path('listCalassrooms/<str:name>/delete/', views.deleteStudent, name='deleteStudent'),
    path('register/', views.RegisterUser, name='register'),
    path('login/', auth_views.LoginView.as_view(template_name='users/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='users/logout.html'), name='logout'),
] 

if settings.DEBUG:       
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
