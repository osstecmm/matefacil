import json
import random

class Graph:

    def __init__(self, json_conf):
        conf = json.loads(json_conf)
        self.deg_v = conf["deg-v"]
        self.weight = conf["weight"]
        self.nodes = random.randint(2, conf["nodes"])
        self.data = [[0 for y in range(self.nodes)] for i in range(self.nodes)]
        self.conections = []
        self.sub = {}
        self.type = conf["type"]
        # self._create_test()
        self._minimalTree()
        # self._splittingSet()

    def _add_and_remove(self, _get, _set):
        data = random.choice(list(_get))
        _get.remove(data)
        _set.add(data)
        return data

    def _create_test(self):
        conn = [
            (0,2),
            (1,3),
            (0,1),
            (1,2),
            (0,4),
            (5,3),
            (4,6),
            (1,7),
            (3,8),
            (7,9),
            (9,9),
            (9,2)
        ]
        for c in conn:
            self.set_edge(c[0], c[1], 1)
        
    def set_edge(self, node_1, node_2, weight):
        if self.type==0 :    
            ma = max(node_1, node_2)
            mi = min(node_1, node_2)
            self.data[ma][mi] = weight
            self.conections.append((ma, mi))
        else:
            self.data[node_1][node_2] = weight
            self.conections.append((node_1, node_2))

    def get_edge(self, node_1, node_2):
        if self.type==0 :    
            ma = max(node_1, node_2)
            mi = min(node_1, node_2)
            return self.data[ma][mi]
        else:
            return self.data[node_1][node_2]

    def _connect_cluster(self, _set):
        unmarked = { i: _set[i].copy() for i in range(len(_set))}
        i_ = random.choice(list(unmarked.keys()))
        temp_ = unmarked[i_].copy()
        unmarked.pop(i_)
        marked = {}
        marked[i_] = set()
        root = self._add_and_remove(temp_, marked[i_])
        while unmarked:
            i = random.choice(list(unmarked.keys()))
            temp = unmarked[i].copy()
            unmarked.pop(i)
            if temp_ is not None :
                unmarked[i_] = temp_
                if len(unmarked[i_])==0 :
                    unmarked.pop(i_)
                temp_ = None
            else :
                ban = False
                if i in marked.keys() :
                    i_ = marked[i].copy()
                    marked.pop(i)
                    ban = True
                root = random.choice(list(random.choice(list(marked.values()))))
                if ban:
                    marked[i] = i_
            if not i in marked.keys() :
                marked[i] = set()
            c = self._add_and_remove(temp, marked[i])
            self.set_edge(root, c, 1)
            unmarked[i] = temp
            if len(unmarked[i])==0 :
                unmarked.pop(i)

    def _minimalTree(self):
        unmarked = set(range(self.nodes))
        marked = set()
        self._add_and_remove(unmarked, marked)
        while unmarked :
            root = random.choice(list(marked))
            c = self._add_and_remove(unmarked, marked)
            self.conections.append((root,c))
            self.set_edge(root,c,1)
    
    def _splittingSet(self):
        # self.sub = [set() for x in range(random.randint(2, self.nodes))]
        self.sub = [set() for x in range(self.nodes//2)]
        unmarked = {n for n in range(self.nodes)}
        self._add_and_remove(unmarked, self.sub[0])
        for sub in range(1, len(self.sub)) :
            self._add_and_remove(unmarked, self.sub[sub])
        while unmarked :
            s = random.choice(self.sub)
            self._add_and_remove(unmarked, s)
        self._connect_cluster(self.sub)

    def toJSON(self):
        print(json.dumps(self.__dict__))

    def printGraph(self):
        for i in range(self.nodes):
            print("[", end="")
            for j in range(self.nodes):
                if i>j or self.type==1:
                    print(self.data[i][j]," ", sep="", end="")
            print("]")
    

    # TODO: Establecer la ruta del archivo aqui
    def toGraphviz(self):
        print(self.sub)
        f = open("./codes/generators/graph/code/Graph.dot", "w")
        self.s = "graph {\n"
        for i in range(len(self.sub)) :
            self.s += " subgraph cluster_"+str(i)+"{\n"
            for s in self.sub[i] :
                self.s += "  "+str(s)+";\n"
            self.s += " }\n"
        for i in range(self.nodes) :
            self.s += " "+str(i)+"\n"
        for i in range(self.nodes) :
            for j in range(self.nodes) :
                if self.data[i][j]!=0 :
                    self.s += " " + str(i) + "--" + str(j) + "\n"
        self.s+="}"
        f.write(self.s)

    def getBridges(self):
        print()

    def getNeighbors(self, actual):
        neighbors = [i for i in range(self.nodes) if self.data[actual][i]==1 or (self.type==0 and self.data[i][actual])]
        return neighbors
