#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cmath>
#include <map>
#include <iostream>

typedef enum operators {_and, _or, _not, _if, _iff, _var} tnode;

typedef struct snode {
    tnode type;
    char prop;
    struct snode *lft;
    struct snode *rgt;
} node;

typedef std::map<char, bool> row;

unsigned int m, n;
node* root;
char alphabet[26];
char usedLetters[5];
char proposition[690];
char answer[81];
char wsp = ' ';

node* newnode(unsigned int level) {
    node* q = new node;
    q->lft = NULL;
    q->rgt = NULL;
    if(level == 0)
        q->type = _var;
    else
        q->type = tnode(rand() % 6);
    if(q->type == _var){
        q->prop = alphabet[rand() % m];
        if(strchr(usedLetters, q->prop) == NULL)
            strncat(usedLetters, &q->prop, 1);
    }
    else
        q->prop = '\0';
    return q;
}
void buildTree(node* p, unsigned int level) {
    switch(p->type) {
        case _and: case _or: case _if: case _iff:
            p->lft = newnode(level - 1);
            buildTree(p->lft, level - 1);
            p->rgt = newnode(level - 1);
            buildTree(p->rgt, level - 1);
            break;
        case _not:
            p->lft = newnode(level - 1);
            buildTree(p->lft, level - 1);
            break;
        case _var:
            return;
        default:
            return;
    }
}
void fillAlphabet() {
    for(int i = 0; i < 26; i++)
        alphabet[i] = i + 97;
}
void unorderAlphabet() {
    int j, k;
    for(int i = 25; i >= 1; i--){
        j = rand() % (i + 1);
        k = alphabet[j];
        alphabet[j] = alphabet[i];
        alphabet[i] = k;
    }
    return;
}
void printExpression(node* p) {
    switch(p->type) {
        case _and: case _or: case _if: case _iff:
            if(p != root)
                strcat(proposition, "( ");
            printExpression(p->lft);
            switch(p->type) {
                case _and:
                    strcat(proposition, "\\wedge ");
                    break;
                case _or:
                    strcat(proposition, "\\vee ");
                    break;
                case _if:
                    strcat(proposition, "\\Rightarrow ");
                    break;
                case _iff:
                    strcat(proposition, "\\Leftrightarrow ");
                    break;
                default:
                    break;
            }
            printExpression(p->rgt);
            if(p != root)
                strcat(proposition, ") ");
            break;
        case _not:
            strcat(proposition, "\\neg ");
            printExpression(p->lft);
            break;
        case _var:
            strncat(proposition, &p->prop, 1);
            strncat(proposition, &wsp, 1);
            return;
        default:
            return;
    }
    return;
}
void orderSubAlphabet(){
    char c;
    int min_index;
    for(int i = 0; i < strlen(usedLetters) - 1; i++){
        min_index = i;
        for(int j = i; j < strlen(usedLetters); j++){
            if(usedLetters[j] < usedLetters[min_index]){
                min_index = j;
            }
        }
        char aux = usedLetters[i];
        usedLetters[i] = usedLetters[min_index];
        usedLetters[min_index] = aux;
    }
}
bool solveTree(node* p, row r){
    switch(p->type){
        case _and:
            return solveTree(p->lft, r) && solveTree(p->rgt, r);
        case _or:
            return solveTree(p->lft, r) || solveTree(p->rgt, r);
        case _not:
            return !solveTree(p->lft, r);
        case _if:
            switch (solveTree(p->lft, r)){
                case true:
                    switch(solveTree(p->rgt, r)){
                        case true:
                            return true;
                        case false:
                            return false;
                    }
                break;
                case false:
                    switch(solveTree(p->rgt, r)){
                        case true:
                            return true;
                        case false:
                            return true;
                    }
                break;
            }
        case _iff:
            switch (solveTree(p->lft, r)){
                case true:
                    switch(solveTree(p->rgt, r)){
                        case true:
                            return true;
                        case false:
                            return false;
                    }
                break;
                case false:
                    switch(solveTree(p->rgt, r)){
                        case true:
                            return false;
                        case false:
                            return true;
                    }
                break;
            }
        case _var:
            return r[p->prop];
        default:
            return false;
    }
}
void generateAnswer(){
    row trueFalse;
    orderSubAlphabet();
    for(int i = 0; i < pow(2, strlen(usedLetters)); i++){
        int decimal = i;
        for(int j = strlen(usedLetters) - 1; j >= 0; j--){
            if(decimal == 0){
                for(int k = 0; k < strlen(usedLetters); k++)
                    trueFalse[usedLetters[k]] = false;
                break;
            } else if(decimal == 1){
                trueFalse[usedLetters[j]] = (bool) decimal;
                break;
            }else{
                trueFalse[usedLetters[j]] = decimal % 2;
                decimal = decimal / 2;
            }
        }
        for(row::iterator it = trueFalse.begin(); it != trueFalse.end(); ++it){
            if(it->second)
                strcat(answer, "1");
            else
                strcat(answer, "0");
        }
        bool result = solveTree(root, trueFalse);
        if(result)
            strcat(answer, "1");
        else
            strcat(answer, "0");
        for(int j = 0; j < strlen(usedLetters); j++)
            trueFalse[usedLetters[j]] = false;
    }
}
void removeTree(node *p){
    switch(p->type) {
        case _and: case _or: case _if: case _iff:
            removeTree(p->lft);
            removeTree(p->rgt);
            delete p;
            return;
        case _not:
            removeTree(p->lft);
            delete p;
            return;
        case _var:
            delete p;
            return;
        default:
            delete p;
            return;
    }
}
int main(int argc, char **argv) {
    srand(time(NULL));
    fillAlphabet();
    m = atoi(argv[1]);
    n = atoi(argv[2]);
    unorderAlphabet();
    root = newnode(n);
    buildTree(root, n);
    printExpression(root);
    generateAnswer();
    printf("%s,%s,%s", usedLetters, proposition, answer);
    removeTree(root);
    return 0;
}
