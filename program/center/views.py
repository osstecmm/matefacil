import  time, datetime 
from django.utils import timezone
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.models import Group, User
from django.contrib.auth.decorators import login_required
from .funtions import *
from .forms import *
from .models import *

# Pagina principal
def index(request):
    print("index()")
    #generateOperationGraph(10)
    return render (request, "app/index.html")

# Pagina para crear un usuario
def RegisterUser(request):
    print("RegisterUser()");
    if request.method == 'POST':
        
        # La base de los datos esta en forms
        form = RegisterForm(request.POST)
        if form.is_valid():
            
            # Se guarda el usuario
            user = form.save()
            group = Group.objects.get(name=request.POST.get('accountType'))
            
            # Guarda el usuario creado en el grupo 'teacher' o 'student'
            user.groups.add(group)
            username = form.cleaned_data.get('username') 
            messages.success(request, f'Se a creado correcta mente el usuario: {username}!. Ya puedes entrar')
            return redirect('login')
    else:
        form = RegisterForm()
    return render(request, 'users/register.html', {'form': form})

# Pagina para editar el usuario
@login_required
def profile(request):
    print("profile()");
    if request.method == 'POST':
        
        # La base de los datos esta en forms
        u_form = UserUpdateFrom(request.POST, instance=request.user)
        #La base de los datos esta en forms
        p_form = ProfileUpdateForm(request.POST, request.FILES, instance=request.user.profile)
        if u_form.is_valid() and p_form.is_valid:
            u_form.save()
            p_form.save()
            username = request.user.username
            messages.info(request, f'Se a actualizador el usuario: {username}!.')
            return redirect('profile')
    else:
        u_form = UserUpdateFrom(instance=request.user)
        p_form = ProfileUpdateForm(instance=request.user.profile)
    #Datos que es pasan a la pagina web
    context = {'u_form': u_form, 'p_form': p_form}
    return render(request, "users/profile.html", context)

# Pagina para cambiar la contraseña
@login_required
def changePass(request):
    print("changePass()");
    if request.method == 'POST':
        
        # La base de los datos esta en forms
        form = PasswordUpdateFrom(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            
            # Importante! Funciona para actualizar la contraseña cifrada
            update_session_auth_hash(request, user) 
            messages.success(request, 'Tu contraseña se a actualizado !')
            return redirect('pass')
        else:
            messages.error(request, 'Por favor, introduce correctamente las contraseñas.')
    else:
        form = PasswordUpdateFrom(request.user)
    return render(request, "users/password.html", {'form': form})

# Eliminar todos los datos de un usuario que se han guardado en la base de datos
@login_required
def deleteUser(request):
    print("deleteUser()");
    query_set = Group.objects.filter(user = request.user)
    for g in query_set:
        a = g.name
        
    # El usario del grupo de 'student'
    if a == "student":
        cr = Classrooms.objects.filter(student_id = request.user.id)
        awa = AveragesWorkActivity.objects.filter(student_id = request.user.id)
        op = OperationsPractics.objects.filter(users = request.user.id)
        owa = OperationsWorkActivity.objects.filter(users = request.user.id)
        com = Comments.objects.filter(user_comment = request.user.id)
        u = User.objects.get(username = request.user.username)
        owa.delete()
        awa.delete()
        com.delete()
        cr.delete()
        op.delete()
        u.delete()
        
    # El usario del grupo de 'student'
    if a == "teacher":
        u = User.objects.get(username = request.user.username)
        c = Classes.objects.filter(teacher_id = request.user.id)
        cr = Classrooms.objects.filter(teacher_id = request.user.id)
        com = Comments.objects.filter(user_comment = request.user.id)
        op = OperationsPractics.objects.filter(users = request.user.id)
        op.delete()
        com.delete()
        for i in c:
            wa = WorkActivity.objects.get(classess_id=i.og)
            og = OperationGenerate.objects.filter(work_activity_id=wa.id)
            awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
            owa = OperationsWorkActivity.objects.filter(work_id=wa.id)
            owa.delete()
            awa.delete()
            og.delete()
            wa.delete()
        cr.delete()
        c.delete()
        u.delete()
    return redirect('index')

# Lista para ver los problemas de practica
@login_required
def listPractices(request):
    print("listPractices()");
    liste = OperationsPractics.objects.filter(users_id=request.user.id)
    
    # Datos que es pasan a la pagina web
    context = {"list": liste}
    return render(request, 'app/listPractices.html', context)

# Pagina para crar problemas de practica
@login_required
def createPractice(request):
    print("createPractice()");
    query = TypeClass.objects.all()
    
    # Datos que es pasan a la pagina web
    context = {"query": query}
    return render (request, "app/createPractice.html", context)

# Es para generar problemas matematicos de pratica
@login_required
def generationPractice(request):
    print("generationPractice()");
    if request.method == 'POST':
        tp = int(request.POST.get("tip"))
        sz = int(request.POST.get("size"))
        ps = int(request.POST.get("propSim"))
        mp = int(request.POST.get("maxProf"))
        op = str(request.POST.get("operation"))
        e = request.POST.getlist("extras")
        con = OperationsPractics.objects.filter(users_id = request.user.id).all().count()
        title = "Operacion " + str(con+1)
        nd = vaildNegativeDecimal(e)
        
        # Arimetica
        if tp == 1:
            # Es para generar problemas llamando la funcion
            goa = generateOperationArithmetic(sz, str(nd), op)
            
            # Es para incertar datos en la base de datos
            OperationsPractics.objects.create(title=title, formula=goa[0], results=goa[1], type_c_id=tp, users_id=request.user.id)
            
        # Desigualdad
        if tp == 2:
            # Es para generar problemas llamando la funcion
            goi = generateOperationInequality(sz)
            
            # Es para incertar datos en la base de datos
            OperationsPractics.objects.create(title=title, formula=goi[0], results=goi[1], type_c_id=tp, users_id=request.user.id)
            
        # Tabla de verdad
        if tp == 3:
            
            # Generar el problema de tabla de verdad
            got = generateOperationTruthTable(ps, mp)
            
            # Es para incertar datos en la base de datos
            OperationsPractics.objects.create(title=title, formula=got[1], results=got[2].strip(), aux=got[0], type_c_id=tp, users_id=request.user.id)
        
        # Grafos (Ruta mas corta)    
        if tp == 4:
            sz = int(request.POST.get("sizeGraph"))
            print("generar grafos generationPractice")
            title=title.replace(' ','')
            print("size= ",sz," title= ",title)
            print("con= ",con)
            problema=generateOperationGraph(sz, request.user.id, title)
            print("problema= ",problema)
            OperationsPractics.objects.create(title=title, formula=str(problema[0])+"-"+str(problema[1]), results=problema[2], aux=0, type_c_id=tp, users_id=request.user.id)
                            


        # Estas 4 lineas de codigo son para obtener el id del problema generado
        l = OperationsPractics.objects.filter(users_id=request.user.id).order_by('-id')[0]
        la = str(l)
        las = list(la.split(" "))
        last = las[2].replace("(", "").replace(")", "")
    return redirect('practice/%d' %int(last), )

# Pagina para responder los problemas de practica
@login_required
def practice(request, pk):
    print("practice()");
    problem = OperationsPractics.objects.get(id = pk)
    print("practice()2");
    typ = OperationsPractics.objects.select_related('type_c').all().filter(id = pk)
    
    # Datos que es pasan a la pagina web
    print("practice()3");
    context = {'p': problem, "type": typ}
    print("practice()4");
    return render (request, "app/practice.html", context)

# Validar las respuesta de los problemas que se han generado
@login_required
def validationPractice(request, pk):
    print("validationPractice()");
    if request.method=='POST':
        res = ""
        if'btn1' in request.POST:
            res = request.POST['respuesta']
        if'btn2' in request.POST:
            res = request.POST['respuesta']
        if'btn3' in request.POST:

            # Obtiene la lista de operadores
            lista=[]
            lista[:]=request.POST['values']

            # Crea el string con formado fila x fila de la tabla del usuario, [vars...] respuesta
            la_buena=''
            for i in range(2**len(lista)):
                for j in range(len(lista)):
                    la_buena+=request.POST[str(i)+str(j)]
                la_buena+=request.POST['r'+str(i)]
            res = la_buena
        r = OperationsPractics.objects.get(id = pk)
        vp = validationProblem(res, r.results)
        OperationsPractics.objects.filter(id=pk).update(validation=vp, your_results=res)

        if 'btnGrafo' in request.POST:
            print("validar practica de grafos")
            r = OperationsPractics.objects.get(id = pk)
            UserRes=request.POST['graphRes']
            UserRes=UserRes.replace(' ','')
            UserRes=UserRes.replace('[','')
            UserRes=UserRes.replace(']','')
            UserRes=[int(s) for s in UserRes.split(',')]
            print("userres.split ",UserRes)
            print("userres.split -1 ",UserRes[-1])
            config= r.results
            evaluarGrafo = Graph.fromJSON(config)
            formula=[int(s) for s in r.formula.split('-')]
            print("formula.split ",formula)
            print("formula.split source ",formula[0])
            print("formula.split target ",formula[-1])
            print("source= ",formula[0]," target= ",formula[-1])
            valid=sp_validation(evaluarGrafo,UserRes,int(formula[0]),int(formula[-1]))
            print("Correct= ",valid)
            deleteGraphSVG(request.user.id,r.title)
            answer=shortest_path(evaluarGrafo, int(formula[0]),int(formula[-1]))
            print("UserRes= ",UserRes)
            print("answer= ",answer)
            if valid == True:
                OperationsPractics.objects.filter(id=pk).update(validation=valid, your_results=UserRes)
            else:
                OperationsPractics.objects.filter(id=pk).update(validation=valid, your_results=UserRes, aux=answer)

    return redirect('listPractices')

# Pagina para crear las clases o grupos
@login_required
def createClass(request):
    print("createClass()");
    query = TypeClass.objects.all()
    
    # Datos que es pasan a la pagina web
    context= {'query': query} 
    
    # Esta funcion es cuando se oprime el boton para crear la clase 
    if request.method == 'POST':
        r1 = request.POST.get('nameClass')
        r3 = int(request.POST.get('tip'))
        gcc = generationCodeClass(request.user.id)
        typeC = TypeClass.objects.get(id=r3)
        tea = User.objects.get(id=request.user.id)
        
        # Es para incertar datos en la base de datos
        Classes.objects.create(name=r1, codes=gcc, teacher=tea, type_c=typeC)
        return redirect('listCalassrooms')
    return render(request, "rooms/createClass.html", context)

# Esta funcion es para validar el codigo generado por los profesores para entrar a los grupos o clases creadas por los profesores
@login_required
def validCode(request):
    print("validCode()");
    if request.method == 'POST':
        r = str(request.POST.get('classIns'))
        if Classes.objects.filter(codes=r):
            Clas = Classes.objects.get(codes=r)
            te = r[10:len(r)]
            tea = User.objects.get(id=te)
            stu= User.objects.get(username=request.user.username)
            Classrooms.objects.create(student=stu, teacher=tea, classess=Clas)
            messages.success(request, "Si, se encontro la clase... ;)")
            return redirect('listCalassrooms')
        else:
            messages.warning(request, "No, se encontro la clase... !")
            return redirect('listCalassrooms')


# Esta muesta una lista de los grupos o clases en los que estars agregado
@login_required
def listCalassrooms(request):
    print("listCalassrooms()");
    query_set = Group.objects.filter(user = request.user)
    for g in query_set:
        a = g.name
    # Esta es la lista para los estudiantes
    if a == "student":
        query = Classrooms.objects.select_related('classess').all().filter(student=request.user.id)
        
        # Esta es la lista para los maestros
    if a == "teacher":
        query = Classes.objects.filter(teacher=request.user.id)
        
        # Datos que es pasan a la pagina web
    context= {'query': query}
    return render(request, "rooms/listClassrooms.html", context)

#Pagina de la clase o grupo
@login_required
def classroom(request, name):
    print("classroom()");
    n = Classes.objects.filter(name=name)
    sc = searchSpecificClass(n)
    n = Classes.objects.get(name=name, teacher_id= sc)
    c = Comments.objects.filter(classess_id = n.id)
    c = c[::-1]
    
    # Es para Hacer un comentrario en la clase o grupo
    if request.method == 'POST':
        m = request.POST.get('msj')
        if m:
            Comments.objects.create(user_comment_id= request.user.id, text=m, classess_id=n.id)
        return redirect('/listCalassrooms/%s' %name)
        
        # Datos que es pasan a la pagina web
    context = {'n':name, 'com': c}
    return render(request, "rooms/classroom.html", context)

# Esta muesta una lista de los estudiantes que han entrado la grupo o clase en el apadtado dentro de miembros
@login_required
def members(request, name):
    print("members()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    tea = str(c.codes)
    teach = tea[10:len(tea)]
    teacher = Classes.objects.select_related('teacher').all().filter(name = name, teacher_id= teach)
    student = Classrooms.objects.select_related('student').all().filter(classess_id=c)
    cou = 12 - Classrooms.objects.filter(classess_id=c.id).count()
    
    # Datos que es pasan a la pagina web
    context = {"tea": teacher, "n": name, "stu":student, "range": range(cou)}
    return render(request, "rooms/members.html", context)

# Lista de las tareas que a creado el maestro
@login_required
def listHomework(request, name):
    print("listHomework()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    listH = WorkActivity.objects.filter(type_a_id=1, classess_id=c.id)
    listH = listH[::-1]
    # Datos que es pasan a la pagina web
    context = {"n": name, 'list': listH}
    return render(request, "rooms/listHomework.html", context)

# Lista de las examenes que a creado el maestro
@login_required
def listExam(request, name):
    print("listExam()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    listE = WorkActivity.objects.filter(type_a_id=2, classess_id=c.id)
    listE = listE[::-1]
    # Datos que es pasan a la pagina web
    context = {"n": name, 'list': listE}
    return render(request, "rooms/listExam.html", context)
 
# Pagina para crear las tareas
@login_required
def createHomework(request,name):
    print("createWork()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    q = TypeActivity.objects.all()
    d = timezone.now().strftime("%Y-%m-%d")
    
    # Datos que es pasan a la pagina web
    context = {"d":d, "query": q, "type":c, "name": name}
    return render(request, "rooms/createHomework.html", context)

# Pagina para crear los examenes
@login_required
def createExam(request,name):
    print("createExam()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    q = TypeActivity.objects.all()
    d = timezone.now().strftime("%Y-%m-%d")
    
    # Datos que es pasan a la pagina web
    context = {"d":d, "query": q, "type":c, "name": name}
    return render(request, "rooms/createExam.html", context)

# Es el generador del molde para para crear los problemas de las tareas o de los examenes 
@login_required
def generationWork(request,name):
    print("generationWork()");
    if request.method == 'POST':
        c = Classes.objects.filter(name=name)
        sc = searchSpecificClass(c)
        c = Classes.objects.get(name=name, teacher_id= sc)
        e = int(request.POST.get('work'))
        n = request.POST.get('name')
        o = request.POST.get('operation')
        ex = request.POST.getlist('extras')
        fh = request.POST.get('bday')
        com = request.POST.get('comment')
        vnd = vaildNegativeDecimal(ex)

        graphQuantity=request.POST.get('quantity')
        graphMaxSize=request.POST.get('size')

        print("cantidad= ",graphQuantity)
        print("tamano= ",graphMaxSize)
            
        if request.POST.get('quantity') is not None:
            q = int(request.POST.get('quantity'))
        else:
            q = 0
        if request.POST.get("size") is not None:
            s = int(request.POST.get("size"))
        else:
            s = 0
        if request.POST.get("propSim") is not None:
            ps = int(request.POST.get("propSim"))
        else:
            ps = 0
        if request.POST.get("maxProf") is not None:
            mp = int(request.POST.get("maxProf"))
        else:
            mp = 0
        
        # Es para incertar datos en la base de datos
        WorkActivity.objects.create(name_work=n, finish_date=fh, type_a_id=e, classess_id=c.id)
        wa = WorkActivity.objects.get(name_work=n, classess_id=c.id)
        
        # Es para incertar datos en la base de datos
        OperationGenerate.objects.create(quantity=q, size=s, propSim=ps, maxProf=mp, operation=o, additional=vnd, work_activity_id=wa.id)
        
        # Tareas
        if e == 1:
            if com != '':
                # Es para incertar datos en la base de datos
                Comments.objects.create(user_comment_id=request.user.id, text= com, classess_id=c.id)
            else:
                t = "Se creo una nueva tarea"
                
                # Es para incertar datos en la base de datos
                Comments.objects.create(user_comment_id=request.user.id, text=t, classess_id=c.id)
            return redirect("/listCalassrooms/%s/listHomework" %name)
            
        # Examnes
        if e == 2:
            if com != '':
                # Es para incertar datos en la base de datos
                Comments.objects.create(user_comment_id=request.user.id, text= com, classess_id=c.id)
            else:
                t = "Se creo un nuevo examen"
                
                # Es para incertar datos en la base de datos
                Comments.objects.create(user_comment_id=request.user.id, text= t, classess_id=c.id)
            return redirect("/listCalassrooms/%s/listExam" %name)
#
@login_required
def work(request, name, name_work):
    print("work()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    # Datos que es pasan a la pagina web
    awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
    awa2 = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id)
    query_set = Group.objects.filter(user = request.user)
    cou = 13 - awa.count()
    away = "0"
    for g in query_set:
        a = g.name
    if a == "student":
        average = AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id)
        # Esta es para entrar a resolver los problemas matemeticos de las tareas despues de haberlos generado antes  
        # Nota: no se puede volver a entar a resolver los examens una ver ya generado los problemas
        if a == "student":
            average = AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id)
            if average:
                away = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=request.user.id)
                if request.method == 'POST':
                    if wa.type_a_id == 1:
                        return redirect(f"/listCalassrooms/{name}/listHomework/{name_work}/homework/")
                    if wa.type_a_id == 2:
                        if away.active == True:
                            return redirect(f"/listCalassrooms/{name}/listExam/{name_work}/")
            # Esto es para generar los problemas matematiaway = "0"cas por cada estudiante para que los resuelva, los problemas se generar en base al molde para generar los problemas.
            else:
                if request.method == 'POST':
                    og = OperationGenerate.objects.get(work_activity_id=wa.id)
                    for o in range(og.quantity):

                        # Arimetica
                        if c.type_c_id == 1:
                            operation = generateOperationArithmetic(int(og.size), og.additional, og.operation)
                            OperationsWorkActivity.objects.create(title=str(o+1), formula=operation[0], results=operation[1], users_id=request.user.id, work_id=wa.id)

                        # Desigualdad
                        if c.type_c_id == 2:
                            operation = generateOperationInequality(int(og.size))
                            OperationsWorkActivity.objects.create(title=str(o+1), formula=operation[0], results=operation[1], users_id=request.user.id, work_id=wa.id)

                        # Tabla de verdad
                        if c.type_c_id == 3:
                            operation = generateOperationTruthTable(int(og.propSim), int(og.maxProf))
                            OperationsWorkActivity.objects.create(title=str(o+1), formula=operation[1], results=operation[2].strip(), aux=operation[0], users_id=request.user.id, work_id=wa.id)

                        if c.type_c_id == 4:
                            print("generar grafos work")
                            print("WORK ID= ",wa.id," title= ",o+1)
                            print("size= ",og.size)
                            problema=generateOperationGraph(og.size, wa.id, o+1)
                            #problema=generateOperationGraph(og.size)
                            print("problema= ",problema)
                            #operation = generateOperationGraph();
                            OperationsWorkActivity.objects.create(title=str(o+1), formula=str(problema[0])+"-"+str(problema[1]), results=problema[2], aux=wa.id, users_id=request.user.id, work_id=wa.id)
                            #OperationsWorkActivity.objects.create(title=str(o+1), formula=str(problema[0])+"->"+str(problema[-2]), results=problema, aux=wa.id, users_id=request.user.id, work_id=wa.id)

                        time.sleep(1)
                    if wa.type_a_id == 1:
                        AveragesWorkActivity.objects.create(average=0, student_id=request.user.id, type_a_id=wa.id)
                        return redirect(f"/listCalassrooms/{name}/listHomework/{name_work}/homework/")
                    if wa.type_a_id == 2:
                        AveragesWorkActivity.objects.create(average=0, student_id=request.user.id, type_a_id=wa.id, active=True)
                        return redirect(f"/listCalassrooms/{name}/listExam/{name_work}/exam/")

        # Datos que es pasan a la pagina web para los estudiantes
        context = {"valid": wa, "awa": awa, "s": away, "name": name , "average": average, "awa2": awa2}
    else:
        # Datos que es pasan a la pagina web para los maestros
        context = {"valid": wa, "awa": awa, 'range': range(cou), "name": name, "name_work": name_work, "awa2": awa2}
    return render(request, "rooms/work.html", context)

#Pagina donde se muestan los problemas matematicos de los examenes o las tareas
@login_required
def homeworkOrExam(request, name, name_work):
    print("homeworkOrExam()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id)
    awa = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=request.user.id)
    owa_c = owa.count()
    
    # Estos if solo son para acomodar los problemas matematicos
    if owa_c <= 3:
        ran = 10
    if owa_c <= 6 and owa_c > 3:
        ran = 8
    if owa_c <= 9 and owa_c > 6:
        ran = 6
    if owa_c <= 12 and owa_c > 9:
        ran = 4
    if owa_c <= 15 and owa_c > 12:
        ran = 2
        
    # Validador si todos problemas estan resueltos
    finish = all(x.validation != None for x in owa)
    context = {"owa":owa, "name": name, "name_work": name_work, "range": range(ran),"finish": finish, "wa": wa}
    
    # Problemas echos para tareas
    if wa.type_a_id == 1:
        return render(request, "rooms/homework.html", context)
        
    # Problemas echos para examens
    else:
        return render(request, "rooms/exams.html", context)

#Validador de los problemas resueltos de tareas o examens
def validHomeworkOrExam(request, name, name_work):
    print("validHomeworkOrExam()");
    if request.method == 'POST':
        c = Classes.objects.filter(name=name)
        sc = searchSpecificClass(c)
        c = Classes.objects.get(name=name, teacher_id= sc)
        wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
        owa = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id)
        owa_c = owa.count()
        print("owa_c= ",owa_c)
        for x in owa:
            if c.type_c_id == 1 or c.type_c_id == 2:
                inp = request.POST.get(f"r{x.title}")
                d = inp
            
            if c.type_c_id == 3:
                # Obtiene la lista de operadores
                lista=[]
                lista[:]=request.POST[f"{x.title}_values"]

                # Crea el string con formado fila x fila de la tabla del usuario, [vars...] respuesta
                la_buena=''
                for i in range(2**len(lista)):
                    for j in range(len(lista)):
                        la_buena+=request.POST[f"{x.title}_"+str(i)+str(j)]
                    la_buena+=request.POST[f"r{x.title}_"+str(i)]
                d = la_buena
            
            if c.type_c_id == 4:
                UserRes=request.POST.get(f"graphRes{x.work_id}_{x.title}")
                print("Respuesta Usuario["+str(x.work_id)+"_"+x.title+"]= "+UserRes)
                #SPRes=x.results
                #SPRes=SPRes.replace(' ','')
                #SPRes=SPRes.replace('[','')
                #SPRes=SPRes.replace(']','')
                #print("Respuesta["+x.aux+"_"+x.title+"]= "+UserRes+"     "+SPRes)
                #print(x.results[1],x.results[-2])
                #config= """{
                #        "type": 0,
                #        "cycle": false,
                #        "deg_v": 2,
                #        "nodes": 10,
                #        "weight": false
                #        }"""
                config= x.results
                evaluarGrafo = Graph.fromJSON(config)
                #d=UserRes==SPRes
                UserRes=UserRes.replace(' ','')
                UserRes=UserRes.replace('[','')
                UserRes=UserRes.replace(']','')
                UserRes=[int(s) for s in UserRes.split(',')]
                print("userres.split ",UserRes)
                print("userres.split -1 ",UserRes[-1])
                formula=[int(s) for s in x.formula.split('-')]
                print("formula.split ",formula)
                print("formula.split source ",formula[0])
                print("formula.split target ",formula[-1])
                print("source= ",formula[0]," target= ",formula[-1])
                valid=sp_validation(evaluarGrafo,UserRes,int(formula[0]),int(formula[-1]))
                print("Correct= ",valid)
                answer=shortest_path(evaluarGrafo, int(formula[0]),int(formula[-1]))
                deleteGraphSVG(x.work_id,x.title)

            #Evaluar respuesta
            # Por cada pregunta contestada correctamente se promeida para sacar un promedio total de la tarea o examen de cada estudiante
            if x.your_results == None and c.type_c_id != 4:
                print("if x.your_results == None and c.type_c_id != 4:")
                vp = validationProblem(d, x.results)
                OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, title=x.title).update(validation=vp, your_results=d)
                if vp == "Correcto":
                    owas = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, validation="Correcto").count()
                    pro = (owas/owa_c)*100
                    AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id).update(average=pro)
            #grafos
            if c.type_c_id == 4 and x.your_results == None:
                print("if c.type_c_id == 4 and x.your_results == None:")
                print("--",valid,"--")
                OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, title=x.title).update(validation=valid, your_results=UserRes, aux=answer)
                if valid == True:
                    owas = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=request.user.id, validation=True).count()
                    pro = (owas/owa_c)*100
                    AveragesWorkActivity.objects.filter(type_a_id=wa.id, student_id=request.user.id).update(average=pro)
                    print("promedio= ",pro," owas= ",owas," owa_c= ",owa_c)

        if wa.type_a_id == 1:
            return redirect(f"/listCalassrooms/{name}/listHomework/{name_work}/homework/")
        else:
            return redirect(f"/listCalassrooms/{name}/listExam/{name_work}/exam/")

#Ver respuestas (grafos)
def viewAnswer(request, name, name_work, student_id):
    print("viewAnswer() - ",request," - ",name," - ",name_work," - ",student_id)
    #if request.method == 'GET':
    c = Classes.objects.filter(name=name)
    print("c= ",c)
    sc = searchSpecificClass(c)
    print("sc= ",sc)
    c = Classes.objects.get(name=name, teacher_id= sc)
    print("c= ",c)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    print("wa= ",wa)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id, users_id=student_id)
    print("owa= ",owa)
    awa = AveragesWorkActivity.objects.get(type_a_id=wa.id, student_id=student_id)
    print("awa= ",awa)
    #return redirect(f"/listCalassrooms/{name}/listHomework/{name_work}/homework/viewAnswer")
    #uno="uno"
    owa_c = owa.count()
    print("owa_c= ",owa_c)
    #awa_c = awa.count()
    #print("awa_c= ",awa_c)

    print(c.type_c_id)
    context = {"owa": owa, "wa": wa, "awa": awa, "name_work": name_work, "name": name}
    return render(request, "rooms/viewAnswer.html", context)

# Eliminar contenido agregado por un estudiante al grupo o clase
def deleteStudent(request, name):
    print("deleteStudent()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    cr = Classrooms.objects.get(classess_id=c.id, student_id=request.user.id)
    wa = WorkActivity.objects.get(classess_id=c.id)
    com = Comments.objects.filter(user_comment=request.user.id, classess_id=c.id)
    awa = AveragesWorkActivity.objects.filter(student_id=request.user.id, type_a_id=wa.id)
    owa = OperationsWorkActivity.objects.filter(users_id=request.user.id, work_id=wa.id)
    com.delete()
    awa.delete()
    owa.delete()
    cr.delete()
    messages.success(request, "Exit the Classrooms")
    return redirect("listCalassrooms")

# Editar tareas o exmanes creados por los profesores
def editHomeworkOrExam(request, name, name_work):
    print("editHomeworkOrExam()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    if request.method == 'POST':
        cn = request.POST.get('changeName')
        WorkActivity.objects.filter(name_work = name_work, classess_id=c.id).update(name_work=cn)
        messages.success(request, "Change Name")
        if wa.type_a_id == 1:
            return redirect(f"/listCalassrooms/{name}/listHomework/")
        if wa.type_a_id == 2:
            return redirect(f"/listCalassrooms/{name}/listExam/")
    context = {"valid": wa,'name': name, "name_work":name_work}
    return render(request, "rooms/editWork.html", context)

# Eliminar tareas o exmanes creados por los profesores
def deleteHomeworkOrExam(request, name, name_work):
    print("deleteHomeworkOrExam()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    wa = WorkActivity.objects.get(name_work = name_work, classess_id=c.id)
    awa = AveragesWorkActivity.objects.filter(type_a_id=wa.id)
    owa = OperationsWorkActivity.objects.filter(work_id=wa.id)
    owa.delete()
    awa.delete()
    wa.delete()
    t = "Delete Work"
    
    # Es para incertar datos en la base de datos
    Comments.objects.create(user_comment_id= request.user.id, text=t, classess_id=c.id)
    messages.success(request, "Delete work")
    return redirect(f"/listCalassrooms/{name}/")

# Editar la clase
def profileClass(request, name):
    print("profileClass()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    if request.method == 'POST':
        cn = request.POST.get('changeName')
        Classes.objects.filter(name=name, teacher_id= f).update(name=cn)
        messages.success(request, "Change Name")
        return redirect('listCalassrooms')
    context = {"class": c, 'name':name}
    return render(request, "rooms/chageClass.html", context)

# Eliminar las clases
def deleteClass(request, name):
    print("deleteClass()");
    c = Classes.objects.filter(name=name)
    sc = searchSpecificClass(c)
    c = Classes.objects.get(name=name, teacher_id= sc)
    cr = Classrooms.objects.filter(classess_id=c.id, teacher_id=request.user.id)
    wa = WorkActivity.objects.filter(classess_id=c.id)
    com = Comments.objects.filter(user_comment=request.user.id, classess_id=c.id)
    com.delete()
    for x in wa:
        awa = AveragesWorkActivity.objects.filter(type_a_id=x.id)
        owa = OperationsWorkActivity.objects.filter(work_id=x.id)
        owa.delete()
        awa.delete()
    wa.delete()
    cr.delete()
    c.delete()