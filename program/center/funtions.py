from sys import stdout, stdin, stderr
import subprocess, random, datetime
from .models import *
import os
import graphviz
import pydot

import json
import random
#from ./codes/generators/graph/code/graph.py import Graph

# Funcion para buscar la clase especifica
def searchSpecificClass(searchClass):
    print("searchSpecificClass()");
    for fil in searchClass:
        sc = fil.codes
        sc = sc[10:len(sc)]
    return sc

# Valida si se selecciono la opcion de numeros negativos o decimales en los generadores de problemas matematicos
def vaildNegativeDecimal(valid):
    print("vaildNegativeDecimal()");
    fun1 = ['n', 'd']
    fun2 = []
    i = 0
    if valid:
        for x in range(len(fun1)):
            if valid[i] == fun1[x]:
                fun2.append(valid[i])
                if i < len(valid) and (i+1) != len(valid):
                        i += 1
            else :
                fun2.append('$')
    else:
        for x in range(len(fun1)):
            fun2.append('$')
    return fun2

# Escoge de manera aleatoria el tipo de operacion
def operationSelect(operation):
    print("operationSelect()");
    if operation == 'aleatory':
        r = int(random.uniform(1, 5))
        if r == 1:
            operative = '+'
        if r == 2:
            operative = '-'
        if r == 3:
            operative = 'z'
        if r == 4:
            operative = '/'
    else:
        operative = operation
    return operative

# Un generador de un tipo de problemas matematicos
def generateOperationArithmetic(sizeOperation, arrayValidNegativeDecimal, typeOperation):
    print("generateOperationArithmetic()");
    typeOperation = operationSelect(typeOperation)
    p = subprocess.Popen([f'java -cp codes/generators/arithmetic/code generateRandomArithmetic {sizeOperation} {arrayValidNegativeDecimal[2]} {arrayValidNegativeDecimal[7]} {typeOperation}'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    s = s.split(' ')
    f = s[0] + " " + s[1] + " " + s[2] + " " + s[3]
    print(f)
    result = [f, s[4]]
    return result

# Otro generador de un tipo de problemas matematicos
def generateOperationInequality(sizeOperation):
    print("generateOperationInequality()");
    p = subprocess.Popen ([f'./codes/generators/inequality/code/generateInequality {sizeOperation}'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    s = s.split(' ')
    f = s[0] + " " + s[1] + " " + s[2]
    result = [f, s[3]]
    return result

# Generador de una proposición y su respectiva tabla de verdad
def generateOperationTruthTable(maxAlphabet, maxDepth):
    print("generateOperationTruthTable()");
    p = subprocess.Popen([f'./codes/generators/truthTable/code/tablasDeVerdad {maxAlphabet} {maxDepth}'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    s = s.split(',')
    result = [s[0], s[1], s[2]]
    return result

#Generador de codigos para que los profesores tengan un cogigo para que los alumnos entren a sus grupos o clases
def generationCodeClass(idUser):
    print("generationCodeClass()");
    L=list('abc1def2gh3ij4kl5mn6opq7r8stu9vw0xyz')
    f=[]
    cl =" "
    for i in range(1):
        p=''
        for j in range(10):
            p+=L[random.randint(0,25)]
        f.append(''.join(p))
    result = cl.join(f) + str(idUser)
    return result

# Suma de tiempo
def sumTime(now, duration):
    print("sumTime()");
    duration = duration.split(':')
    now = datetime.datetime.strptime(now, "%X" )
    d = datetime.timedelta(hours=int(duration[0]), minutes=int(duration[1]))
    sum = now + d
    return sum

# El validador simple de problemas matematios
def validationProblem(answer, result):
    print("validationProblem()");
    p = subprocess.Popen ([f'java -cp codes/validation/simple validation {answer} {result}'], stdout=subprocess.PIPE, shell = True)
    s = p.communicate()[0].decode('utf-8')
    return s

# Convertidor de string a array
def convertList(dateToList):
    print("convertList()");
    resutlt = dateToList.split(':')
    return resutlt


# INICIO FUNCIONES PARA LAS ACTIVIDADES DE GRAFOS
#//////////////////////////////////////////////////////////////////////////////////////
#//////////////////////////////////////////////////////////////////////////////////////
#//////////////////////////////////////////////////////////////////////////////////////
# INICIO FUNCIONES PARA LAS ACTIVIDADES DE GRAFOS 

# Funcion para generar el .dot
def generarArchivoDot(size):
    print("Inicio funcion para generar el archivo .dot")
    print("grado del grafo= ",size)
    #config= """{
    #        "type": 0,
    #        "cycle": false,
    #        "deg_v": 1,
    #        "nodes": 10,
    #        "weight": false
    #        }"""

    grafo = Graph(nodes=int(size),deg_v=2)
    #grafo = Graph(int(size),2,0)
    #grafo = Graph(10,2,0)
    #grafo = Graph.fromJSON(config)

    grafo.toGraphviz()
    

    print("Fin funcion para generar el archivo .dot")
    ruta=generateShortestPathProblem(size)
    print("origen = ",ruta[0]," fin = ",ruta[-1])
    #return shortest_path(grafo,ruta[0],ruta[-1])
    contenido=["","",""]
    contenido[0]= str(ruta[0])
    contenido[1]= str(ruta[-1])
    contenido[2]= grafo.toJSON()
    print("JSON1= ",grafo.toJSON())
    print("JSON2= ",contenido[2])
    #return shortest_path(grafo,ruta[0],ruta[-1])
    return contenido
# Fin funcion para generar archivo .dot

# Ejecuta DOT de GRAPHVIZ para generar la representacion del grafo en formato SVG a partir del grafo descrito en el archivo .dot
#generateOperationGraph(size):
def generateOperationGraph(size,workId,problemId):
    print("Inicio funcion para generar representacion svg del grafo")

    res=generarArchivoDot(size) #arreglo que contiene la respuesta
    ruteCompileGraphViz="dot -Tsvg ./codes/generators/graph/code/Graph.dot -o ./center/templates/static/graph/Graph"+str(workId)+"_"+str(problemId)+".svg"

    os.system(ruteCompileGraphViz)
    #os.system("dot -Tsvg ./codes/generators/graph/code/Graph.dot -o ./center/templates/static/graph/output.svg")

    #rute=generateShortestPathProblem(size) #arreglo que cotiene la ruta del problema
    print("respuesta= ",res)
    print("Fin funcion para generar representacion svg del grafo")
    return res

    #graph = pydot.graph_from_dot_file('codes/generators/graph/code/Graph.dot')
    #graph = graph[0]
    #graph.write_png('codes/generators/graph/code/output.png')

    #p = subprocess.Popen([f'./codes/generators/graph/code/Graph.py'], stdout=subprocess.PIPE, stdin=subprocess.PIPE, shell = True)
    #s = p.communicate()[0].decode('utf-8')
    #s = s.split(' ')
    
    #os.system("dot -Tsvg ./codes/generators/graph/code/Graph.dot -o ./codes/generators/graph/code/output.svg")

# Fin generateOperationGraph()

def deleteGraphSVG(workId,problemId):
    fileName="./center/templates/static/graph/Graph"+str(workId)+"_"+str(problemId)+".svg"
    if os.path.exists(fileName):
        #os.remove(fileName)
        print("El Archivo ",fileName," se elimino")
    else:
        print("El Archivo ",fileName," no existe")

def generateShortestPathProblem(size):
    nodos = [i for i in range(size)]

    source= random.choice(nodos)
    nodos.remove(source)
    target= random.choice(nodos)

    return [source,target]
    


class Graph:

    def __init__(self, nodes, deg_v=1, weight=False, type_g=0, create = True, con = None):
        print("Constructor. Grado= ",nodes)
        self.deg_v = deg_v
        self.weight = weight
        # self.nodes = random.randint(2, nodes)
        self.nodes = nodes
        self.data = [[0 for y in range(self.nodes)] for i in range(self.nodes)]
        self.connections = []
        self.sub = {}
        self.type = type_g
        if con :
            print("Load Graph")
            for c in con :
                self.set_edge(c[0], c[1], 1)
        if create:
            #self._create_test()
            self._minimalTree()
            # self._splittingSet()

    @classmethod
    def fromGraph(cls, g):
        return cls(g.nodes, g.deg_v, g.weight, g.type, False)

    @classmethod
    def fromJSON(cls, json_conf):
        conf = json.loads(json_conf)
        if not "connections" in conf :
            conf["connections"] = None
        return cls(conf["nodes"], conf["deg_v"], conf["weight"], conf["type"], False, conf["connections"])

    def _add_and_remove(self, _get, _set):
        data = random.choice(list(_get))
        _get.remove(data)
        _set.add(data)
        return data

    def _create_test(self):
        conn = [
            (0,2),
            (0,5),
            (1,2),
            (1,3),
            (1,4),
            (2,4),
            (2,7),
            (3,2),
            (3,4),
            (3,5),
            (3,6),
            (4,5),
            (5,9),
            (7,8)
        ]
        for c in conn:
            self.set_edge(c[0], c[1], 1)
        
    def set_edge(self, node_1, node_2, weight):
        if self.type==0 :    
            ma = max(node_1, node_2)
            mi = min(node_1, node_2)
            self.data[ma][mi] = weight
            self.connections.append((ma, mi))
        else:
            self.data[node_1][node_2] = weight
            self.connections.append((node_1, node_2))

    def get_edge(self, node_1, node_2):
        if self.type==0 :    
            ma = max(node_1, node_2)
            mi = min(node_1, node_2)
            return self.data[ma][mi]
        else:
            return self.data[node_1][node_2]

    def _connect_cluster(self):
        unmarked = { i: self.sub[i].copy() for i in range(len(self.sub))}
        i_ = random.choice(list(unmarked.keys()))
        temp_ = unmarked[i_].copy()
        unmarked.pop(i_)
        marked = {}
        marked[i_] = set()
        root = self._add_and_remove(temp_, marked[i_])
        weight = 1
        while unmarked:
            i = random.choice(list(unmarked.keys()))
            temp = unmarked[i].copy()
            unmarked.pop(i)
            if temp_ is not None :
                unmarked[i_] = temp_
                if len(unmarked[i_])==0 :
                    unmarked.pop(i_)
                temp_ = None
            else :
                ban = False
                if i in marked.keys() :
                    i_ = marked[i].copy()
                    marked.pop(i)
                    ban = True
                root = random.choice(list(random.choice(list(marked.values()))))
                if ban:
                    marked[i] = i_
            if not i in marked.keys() :
                marked[i] = set()
            c = self._add_and_remove(temp, marked[i])
            if self.weight :
                weight = random.randint(10, 20)
            self.set_edge(root, c, weight)
            unmarked[i] = temp
            if len(unmarked[i])==0 :
                unmarked.pop(i)

    def _minimalTree(self):
        unmarked = set(range(self.nodes))
        marked = set()
        self._add_and_remove(unmarked, marked)
        weight = 1
        while unmarked :
            root = random.choice(list(marked))
            c = self._add_and_remove(unmarked, marked)
            self.connections.append((root,c))
            if self.weight :
                weight = random.randint(10, 20)
            self.set_edge(root,c,weight)
        for m in marked:
            n = self.getNeighbors(m)
            while len(n)<self.deg_v:
                unmarked = set(marked) - set(n + [m])
                print("unmarked= ",unmarked)
                new_n = random.choice(list(unmarked))
                n.append(new_n)
                if self.weight :
                    weight = random.randint(10, 20)
                self.set_edge(m, new_n, weight)
    
    def _splittingSet(self):
        # self.sub = [set() for x in range(random.randint(2, self.nodes))]
        self.sub = [set() for x in range(self.nodes//2)]
        unmarked = {n for n in range(self.nodes)}
        self._add_and_remove(unmarked, self.sub[0])
        for sub in range(1, len(self.sub)) :
            self._add_and_remove(unmarked, self.sub[sub])
        while unmarked :
            s = random.choice(self.sub)
            self._add_and_remove(unmarked, s)
        self._connect_cluster()

    def toJSON(self):
        temp = self.data
        self.data = None
        s = json.dumps(self.__dict__)
        self.data = temp
        return s.replace(" ", "")

    def printGraph(self):
        for i in range(self.nodes):
            print("[", end="")
            for j in range(self.nodes):
                if i>j or self.type==1:
                    print(self.data[i][j]," ", sep="", end="")
            print("]")
    

    # TODO: Establecer la ruta del archivo aqui
    def toGraphviz(self):
        print("toGraphviz()")
        f = open("./codes/generators/graph/code/Graph.dot", "w")
        s = "graph {\n" if self.type == 0 else "digraph {\n"
        for i in range(len(self.sub)) :
            s += " subgraph cluster_"+str(i)+"{\n"
            for s in self.sub[i] :
                s += "  "+str(s)+";\n"
            s += " }\n"
        for i in range(self.nodes) :
            s += " "+str(i)+"\n"
        for i in range(self.nodes) :
            for j in range(self.nodes) :
                if self.data[i][j]!=0 :
                    s += " " + str(i) + ("--" if self.type==0 else "->") + str(j) + "\n"
        s+="}"
        f.write(s)

    def getBridges(self):
        print()

    def getNeighbors(self, actual):
        neighbors = [i for i in range(self.nodes) if self.data[actual][i]==1 or (self.type==0 and self.data[i][actual])]
        return neighbors

#

#Funciones para validacion de problemas de grafos

def is_connected(g, first, ignore = None):
    """Verifica si un grafo es completamente conexo.

    Devuelve un valor booleano de acuerdo a si es
    totalmente conexo, el parametro "first" 
    determina el primer nodo a visitar, si ignore es
    incluido ese vertice no se tomara en cuenta al 
    validar las conexiones.

    Keyword arguments:  
    g -- Objeto de tipo grafo. type: Graph.  
    first -- Primer nodo del grafo en visitar. type: int  
    ignore -- Nodo a excluir en la busqueda de conexion
    (default None). type: int or None.

    return bool value. True if is connected,
    False in else case. 

    Excepciones:
    IndexError -- Si la matriz de adyaciencia no esta inicializada.
    
    """
    unvisited = { i for i in range(g.nodes)}
    to_visit = [first]
    index = 0
    while index<len(to_visit):
        neighbor = g.getNeighbors(to_visit[index])
        unvisited.remove(to_visit[index])
        to_visit.extend(n for n in neighbor if n not in to_visit)
        index += 1
    if ignore in unvisited:
        unvisited.remove(ignore)
    return len(unvisited)==0

def aristas_puente(g):
    """Obtiene un lista de tuplas de vertices.

    Devuelve un lista de tuplas de vertices, es decir una arista,
    se implementa una busqueda en profundidad para saber si es
    completamente conexo.

    Keyword arguments:  
    g -- Objeto de tipo grafo. type: Graph

    return list(tuple(int, int)). Lista de aristas

    Excepciones:
    IndexError -- Si la matriz de adyaciencia no esta inicializada.

    """
    vertex = []
    for i in range(g.nodes):
        neighbors = g.getNeighbors(i)
        for n in neighbors:
            if (n,i) in vertex:
                continue
            g.set_edge(i, n, 0)
            if not is_connected(g, i):
                vertex.append((i, n))
            g.set_edge(i, n, 1)
    return vertex

def vertices_corte(g):
    """Obtiene un lista de nodos de corte.

    Devuelve un lista de vertices, se implementa
    una busqueda en profundidad para saber si es
    completamente conexo.

    Keyword arguments:  
    g -- Objeto de tipo grafo. type: Graph

    Return:  
    list(int). Lista de vertices

    Excepciones:
    IndexError -- Si la matriz de adyaciencia no esta inicializada.

    """
    nodes = []
    for i in range(g.nodes):
        neighbors = g.getNeighbors(i)
        list_w = {}
        # TODO: Obtener valores de las conexiones
        for n in neighbors:
            list_w[n] = g.get_edge(i, n)
            g.set_edge(i, n, 0)
        # Si hay ciclos, previene que se tome a si mismo
        temp = random.choice([p for p in neighbors if p!=i])
        if not is_connected(g, temp, i):
            nodes.append(i)
        for n in neighbors:
            g.set_edge(i, n, list_w[n])
    return nodes

def DFS(g, source, target):
    """Obtiene un pila de vertices.

    DFS: hace una busqueda de un camino del nodo "source"
    al nodo "target", si hay una ruta devuelve el "camino"
    (la traza de nodos), si no lo hay devuelve una lista vacia.

    Keyword arguments:  
    g -- Objeto de tipo grafo. type: Graph
    source -- nodo donde inicia la busqueda. type: int
    source -- nodo a buscar. type: int

    Return:  
    list(int). Pila de vertices

    Excepciones:
    IndexError -- Si la matriz de adyaciencia no esta inicializada.

    """
    stack = [source]
    mark_set = set()
    while stack :
        u = stack[0]
        mark_set.add(u)
        N = list(set(g.getNeighbors(u))-mark_set)
        if N:
            v = N[0]
            stack.insert(0,v)
            if v == target:
                return stack[::-1]
        else:
            stack.pop(0)
    return stack

def shortest_path(g, source, target):
    """Obtiene la ruta más corta entre dos vertices.

    Shortest Path: hace una busqueda de la ruta mas corta del nodo "source"
    al nodo "target", si hay una ruta devuelve el "camino"
    (la traza de nodos), si no lo hay devuelve una lista vacia.

    Keyword arguments:  
    g -- Objeto de tipo grafo. type: Graph
    source -- nodo donde inicia la busqueda. type: int
    source -- nodo a buscar. type: int

    Return:  
    list(int). Pila de vertices

    Excepciones:
    IndexError -- Si la matriz de adyaciencia no esta inicializada.

    """
    queue=[source]
    marked_set = {source}
    temp_g = g.fromGraph(g)
    while queue :
        u = queue[0]
        queue.pop(0)
        for v in list(set(g.getNeighbors(u))-marked_set):
            temp_g.set_edge(u, v, g.get_edge(u,v))
            if v == target:
                return DFS(temp_g, source, target)
            queue.append(v)
            marked_set.add(v)
    return queue

def sp_validation(g, res, source, target):
    sp = shortest_path(g, source, target)
    if len(res) != len(sp) or res[0]!=source or res[-1]!=target:
        print("Bad size or bad init or end")
        return False
    if res == sp :
        return True
    for i in range(len(res)) :
        start_n = res[i]
        if i==len(res)-1 and res[i]==target :
            return True
        next_n = res[i+1]
        if (not 0<=start_n<g.nodes) or (not 0<=next_n<g.nodes):
            print("Bad index", start_n, next_n)
            return False
        w = g.get_edge(start_n, next_n)
        if w==0 :
            print("Edge dont exist")
            return False

####################################







# FIN FUNCIONES PARA LAS ACTIVIDADES DE GRAFOS
#//////////////////////////////////////////////////////////////////////////////////////
#//////////////////////////////////////////////////////////////////////////////////////
#//////////////////////////////////////////////////////////////////////////////////////
# FIN FUNCIONES PARA LAS ACTIVIDADES DE GRAFOS


#mostrar respuestas