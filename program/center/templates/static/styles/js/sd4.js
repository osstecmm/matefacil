import { renderMathInDocument } from "./MathLive"

function arithmetic() {

    var a = document.getElementById("tamano1");
    a.style.display = "block";
    var b = document.getElementById("tamano2");
    b.style.display = "none";
    var c = document.getElementById("myDIV");
    c.style.display = "block";
    var d = document.getElementById("myDIVS");
    d.style.display = "none";
}

function inequality() {
    var a = document.getElementById("tamano1");
    a.style.display = "none";
    var b = document.getElementById("tamano2");
    b.style.display = "none";
    var c = document.getElementById("myDIV");
    c.style.display = "none";
    var d = document.getElementById("myDIVS");
    d.style.display = "block";
    document.getElementById("myDIVS").innerHTML = "<br>";
}

function truthtable() {
    var a = document.getElementById("tamano1");
    a.style.display = "none";
    var b = document.getElementById("tamano2");
    b.style.display = "block";
    var c = document.getElementById("myDIV");
    c.style.display = "none";
    var d = document.getElementById("myDIVS");
    d.style.display = "none";
}

function changeFunc() {
    var selectBox = document.getElementById("selectBox");
    var selectedValue = selectBox.options[selectBox.selectedIndex].value;
    if (selectedValue === '1') {
        arithmetic();
    }
    if (selectedValue === '2') {
        inequality();
    }
    if (selectedValue === '3') {
        truthtable();
    }
}

function startWork() {
    var x = confirm("Ready, set, go!");
    if (x)
        return true;
    else
        return false;
}


function preparar() {
    console.log("Preparando...");
    let variables = Array.from(document.getElementById("values").value);
    let tablaVerdad = new Array(variables.length);
    let contenido = ``;

    tablaVerdad.fill(false);

    variables.forEach((element) => {
        contenido += `<div tabindex="0" class="col bg-info text-white text-center p-2" > $$${element}$$ </div>`;
    });

    contenido += `<div tabindex="0" class="col bg-info text-white text-center p-2" > $$Respuesta$$ </div>`;

    // console.log(variables.length);
    for (let i = 0; i < 2 ** variables.length; i++) {
        contenido += ` <div class="w-100"></div>`;

        for (let j = 0; j < variables.length; j++) {
            contenido += `<div class="col my-form-control-array text-center"><input type="hidden" name="${i}${j}" value="${tablaVerdad[variables.length - j - 1] ? "1" : "0"
                }" />${tablaVerdad[variables.length - j - 1] ? "$$Verdadero$$" : "$$Falso$$"
                }</div>`;
        }

        for (let index = 0; index < tablaVerdad.length; index++) {
            if (tablaVerdad[index]) {
                tablaVerdad[index] = false;
            } else {
                tablaVerdad[index] = true;
                break;
            }
        }

        contenido += `<select class="custom-select col my-form-control-array text-center" name="r${i}" required > <option class="text-muted" value="" selected> Elige... </option> <option class="font-weight-bold text-success" value="1"> Verdadero </option> <option class="font-weight-bold text-danger" value="0"> Falso </option> </select>`;
    }

document.getElementById("tabla").innerHTML = contenido;
    renderMathInDocument();
};