import time
from datetime import datetime
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.

class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    image = models.ImageField(default='default.jpg', upload_to='profile_pics')
    # Funcion para que se mestre en la parte de admistarador el nombre de usuario en ves de tener el nombre de objeto
    def __str__(self):
        return f'{self.user.username} Profile'

class TypeClass(models.Model):
    name = models.CharField(max_length=50)

class Classes(models.Model):
    name = models.CharField(max_length=200)
    codes = models.CharField(max_length=20, unique = True)
    teacher = models.ForeignKey(User, blank=True, related_name='teacher', default=None, on_delete=models.CASCADE)
    type_c = models.ForeignKey(TypeClass, null=True, default=None, on_delete=models.CASCADE)

class Classrooms(models.Model):
    teacher = models.ForeignKey(User, blank=True, related_name='teachers', default=None, on_delete=models.CASCADE)
    student = models.ForeignKey(User, blank=True, related_name='students', default=None, on_delete=models.CASCADE)
    classess = models.ForeignKey(Classes, null=True, default=None, on_delete=models.CASCADE)

class TypeActivity(models.Model):
    name = models.CharField(max_length=50)

class WorkActivity(models.Model):
    name_work = models.CharField(max_length=50)
    type_a = models.ForeignKey(TypeActivity, null=True, default=None, on_delete=models.CASCADE)
    start_date = models.DateField(default=timezone.now().strftime("%Y-%m-%d"))
    finish_date = models.DateField()
    classess = models.ForeignKey(Classes, null=True, default=None, on_delete=models.CASCADE)
    # Funcion para determinar un limite de fecha entre la fecha de inicio y fin para resolver los problemas de tarea o examen
    def active(self):
        now = timezone.now().strftime("%Y-%m-%d")
        date_object = datetime.strptime(now, "%Y-%m-%d" ).date()
        if self.start_date <= date_object and date_object <= self.finish_date:
            return True
        return False

class OperationGenerate(models.Model):
    quantity = models.IntegerField()
    size = models.IntegerField(blank=True, null=True, default=None)
    propSim = models.IntegerField(blank=True, null=True, default=None)
    maxProf = models.IntegerField(blank=True, null=True, default=None)
    operation = models.CharField(max_length=10, blank=True, null=True, default=None)
    additional = models.CharField(max_length=10, blank=True, null=True, default=None)
    work_activity = models.ForeignKey(WorkActivity, null=True, default=None, on_delete=models.CASCADE)

class AveragesWorkActivity(models.Model):
    average = models.IntegerField()
    student = models.ForeignKey(User, blank=True, related_name='studentWork', null=True, default=None, on_delete=models.CASCADE)
    type_a = models.ForeignKey(WorkActivity, null=True, default=None, on_delete=models.CASCADE)
    active = models.BooleanField(null=True, default=False, blank=True)

class OperationsPractics (models.Model):
    title = models.CharField(max_length=50)
    formula = models.CharField(max_length=225)
    results = models.CharField(max_length=511)
    aux = models.CharField(max_length=225, null=True, blank=True)
    your_results = models.CharField(max_length=225, null=True, blank=True)
    validation = models.CharField(max_length=20, null=True, blank=True)
    users = models.ForeignKey(User, blank=True, related_name='user', default=None, on_delete=models.CASCADE)
    type_c = models.ForeignKey(TypeClass, null=True, default=None, on_delete=models.CASCADE)

class OperationsWorkActivity (models.Model):
    title = models.CharField(max_length=50)
    formula = models.CharField(max_length=225)
    results = models.CharField(max_length=511)
    aux = models.CharField(max_length=225, null=True, blank=True)
    validation = models.CharField(max_length=20, null=True, blank=True)
    your_results = models.CharField(max_length=225, null=True, blank=True)
    users = models.ForeignKey(User, blank=True, related_name='studentOpeWork', default=None, on_delete=models.CASCADE)
    work = models.ForeignKey(WorkActivity, null=True, default=None, on_delete=models.CASCADE)

class Comments(models.Model):
    user_comment = models.ForeignKey(User, blank=True, related_name='users', default=None, on_delete=models.CASCADE)
    time = models.CharField(max_length=50, default=timezone.now().strftime("%a %b %d %Y, %H:%M:%S"))
    text = models.TextField()
    classess = models.ForeignKey(Classes, null=True, default=None, on_delete=models.CASCADE)
    # Funcion para que es puedan guandar archivos de texto grande en la base de datos
    def __str__(self):
        return self.text
