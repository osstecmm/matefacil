from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, PasswordChangeForm
from .models import Profile

# La base de los datos que pide para agragar un nuevo usuario
class RegisterForm(UserCreationForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['first_name', 'last_name', 'username', 'email', 'password1', 'password1']
        labels = {'first_name': 'Fist Name', 'last_name': 'Last Name', 'username': 'No. Control', 'email': 'Email', 'password1': 'Password', 'password1': 'Confirm Password'}
    
    # Funcion para detectar si existe un usuario con el mismo nombre
    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)
        if r.count():
            raise  ValidationError("Username already exists")
        return username

    # Funcion para detectar si existe un usurio con el mismo correo electronico
    def clean_email(self):
        email = self.cleaned_data['email'].lower()
        r = User.objects.filter(email=email)
        if r.count():
            raise  ValidationError("Email already exists")
        return email

# La base de los datos que pide para actualizar datos personales
class UserUpdateFrom(forms.ModelForm):
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ['username', 'first_name', 'last_name', 'email']
        labels = {'username': 'Username', 'first_name': 'Fist Name', 'last_name': 'Last Name', 'email': 'Email'}

# La base de los datos que pide para actualizar la foto del usuario
class ProfileUpdateForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ['image']
        
# La base de los datos que pide para actualizar la contraseña 
class PasswordUpdateFrom(PasswordChangeForm):
    class Meta:
        model = User
        fields = ['old_password', 'new_password1', 'new_password2']